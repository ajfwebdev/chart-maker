// Global Variables declared in PkChart-globals.js

// This file contains Objects and Constructors for the Chart Editor and Chart Creator/Updater

// The Chart Object represents a chart on the editor page
               
function Chart(chartId, chartDivId, chartSvgId, chartGId, svgCssClass, chartTitle, chartXaxisLine, chartYaxisLine, chartItems, chartItemInterval, chartTitleX, chartTitleY, chartPositionX, chartPositionY, chartWidth, chartHeight, moveable) {
  this.chartId = chartId;
  this.chartDivId = chartDivId;
  this.chartSvgId = chartSvgId;
  this.chartGId = chartGId;
  this.svgCssClass = svgCssClass;
  this.chartTitle = chartTitle;
  this.chartXaxisLine = chartXaxisLine;
  this.chartYaxisLine = chartYaxisLine;
  this.chartItems = chartItems;  //an array of chart item objects
  this.chartItemInterval = chartItemInterval;
  this.chartTitleX = chartTitleX;
  this.chartTitleY = chartTitleY;
  this.chartPositionX = chartPositionX;
  this.chartPositionY = chartPositionY;
  this.chartWidth = chartWidth;
  this.chartHeight = chartHeight;
  this.moveable = moveable;
}

Chart.prototype.displayChart = function () {
    // First add the svg node as child of the parent div:
    this.createSvgChild();
    // Add a "g" node below the svg to group all the graphics together (so they can be transformed as a unit)
    this.createGchild();

    var linesCssClass = "chart-lines";
    var textCssClass = "chart-text";
    // Add child nodes to above svg node for the default chart layout
    addSvgLineNode(this.chartGId, linesCssClass, this.chartXaxisLine);
    addSvgLineNode(this.chartGId, linesCssClass, this.chartYaxisLine);
    addSvgTextNode(this.chartGId, textCssClass, this.chartTitleX, this.chartTitleY, this.chartTitle);

    for (var i = 0; i < this.chartItems.length; i++) {
        addSvgTextNode(this.chartGId, textCssClass, this.chartItems[i].itemNameX, this.chartItems[i].itemNameY, this.chartItems[i].itemName);

        // Call the function to add the icon row, depending on the type of icon being added.
        if (this.chartItems[i].itemIconId && this.chartItems[i].itemNumValue) {
            switch (this.chartItems[i].itemIconId) {
                case "icon-1":
                    addCircleIconRow(this.chartGId, this.chartItems[i]);
                    break;
                case "icon-2":
                    addSquareIconRow(this.chartGId, this.chartItems[i]);
                    break;
                case "icon-3":
                    addTriangleIconRow(this.chartGId, this.chartItems[i]);
                    break;
                default:
                    console.log("unhandled icon ID in displayChart function!");
            } // switch
        } // if
    } // for

}

Chart.prototype.createGchild = function () {
    // Create the g node under the chart svg node:
    var parentSvgNode = document.getElementById(this.chartSvgId);
    var myGnode = document.createElementNS("http://www.w3.org/2000/svg", "g");
    myGnode.setAttribute("id", this.chartGId);
    parentSvgNode.appendChild(myGnode);
}

Chart.prototype.createSvgChild = function () {
    // Create the SVG node under parent div node of this chart:
    var parentDivNode = document.getElementById(this.chartDivId);
    var mySvgNode = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    mySvgNode.setAttribute("xmlns", "http://www.w3.org/2000/svg");
    mySvgNode.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
    mySvgNode.setAttribute("id", this.chartSvgId);
    mySvgNode.setAttribute("class", this.svgCssClass);
    mySvgNode.setAttribute("width", this.chartWidth);
    mySvgNode.setAttribute("height", this.chartHeight);
    mySvgNode.setAttribute("x", this.chartPositionX);
    mySvgNode.setAttribute("y", this.chartPositionY);
    if (this.moveable) {
        mySvgNode.setAttribute("transform", "matrix(1 0 0 1 0 0)"); // initialize with identity matrix
        mySvgNode.setAttribute("onmousedown", "selectElement(evt)");
    }
    parentDivNode.appendChild(mySvgNode);
/* Creates a node like: <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
    id="chart-preview-svg" width="400" height="301"></svg> */
}

Chart.prototype.calcIconRowStartX = function () {
    // Calculate the starting X coordinate for this row of icons.
    var x_num = Number(this.chartXaxisLine.x1);
    x_num += 10;
    var rowStart_x = x_num.toString();
    return rowStart_x;  
}
Chart.prototype.calcItemPosY = function (itemSeqNbr) {
    var itemInterval = Number(this.chartItemInterval); 
    var multiplier = Number(itemSeqNbr);
    var y = itemInterval * multiplier;
    return y;   
}


Chart.prototype.addChartItem = function (newChartItem) {
    // If an item with the same sequence number already exists, then copy property values from new to old.
    var replaceItem = false;
    for (var i = 0; i < this.chartItems.length; i++) {
        if (this.chartItems[i].itemSeqNbr === newChartItem.itemSeqNbr) {
            replaceItem = true;
            this.chartItems[i].itemName = newChartItem.itemName;
            this.chartItems[i].itemNameX = newChartItem.itemNameX;
            this.chartItems[i].itemNameY = newChartItem.itemNameY;
            this.chartItems[i].itemNumValue = newChartItem.itemNumValue;
            this.chartItems[i].itemIconId = newChartItem.itemIconId;
            this.chartItems[i].itemIconColorId = newChartItem.itemIconColorId;
            this.chartItems[i].iconRowStartX = newChartItem.iconRowStartX;
            this.chartItems[i].iconRowStartY = newChartItem.iconRowStartY;
            break;
        }
    }

    // If the new ChartItem doesn't exist yet, then add it to the array:
    if (replaceItem === false) {
        this.chartItems.push(newChartItem);
    }
};


// The Chart Item Object represents an item (or row with data and icons) in a Chart
function ChartItem (itemSeqNbr, itemName, itemNameX, itemNameY, itemNumValue, itemIconId, itemIconColorId, iconRowStartX, iconRowStartY) {
  this.itemSeqNbr = itemSeqNbr;
  this.itemName = itemName;
  this.itemNameX = itemNameX;
  this.itemNameY = itemNameY;
  this.itemNumValue = itemNumValue;
  this.itemIconId = itemIconId;
  this.itemIconColorId = itemIconColorId;
  this.iconRowStartX = iconRowStartX;
  this.iconRowStartY = iconRowStartY;
}
