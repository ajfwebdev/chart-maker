// Global Variables declared in PkChart-globals.js

// Handle Top Controls
document.getElementById("chart-add-button").onclick = openCreatorWindowAdd;
//document.getElementById("chart-update-button").onclick = openCreatorWindowUpdate;

// Handle Chart Submission
document.getElementById("chart-submit-button").onclick = submitChart;

function submitChart() {
    // display the current chart in the editor
    currentChart.chartDivId = chartEditorDivId;
    currentChart.chartSvgId = "chart-svg-" + currentChart.chartId;
    currentChart.chartGId = "chart-g-" + currentChart.chartId;
    currentChart.svgCssClass = "draggable";
    currentChart.chartPositionX = "0";
    currentChart.chartPositionY = "0";
    currentChart.moveable = true;
    globalChartArray.push(currentChart); // add this chart to the global chart array - to display in the editor

    currentChart.displayChart();
    //displayChart(currentChart.chartDivId, currentChart.chartSvgId, currentChart.svgCssClass, currentChart.chartWidth, currentChart.chartHeight, currentChart.chartPositionX, currentChart.chartPositionY);
    closeCreatorWindow();
}

function openCreatorWindowAdd() {
    newChart = true;
    // Create a new Default Chart object and display it in the preview window of the creator/updater:
    chartId++;
    var chartDivId = chartPreviewDivId;
    var chartSvgId = chartPreviewSvgId;
    var chartGId = chartPreviewGId;
    var svgCssClass = "chart-preview-svg";
    var chartTitle = "Title";
    var chartXaxisLine = chartDefaultXaxisLine;
    var chartYaxisLine = chartDefaultYaxisLine;
    var chartTitleX = "100";
    var chartTitleY = "10";
    var chartItems = [];
    var chartItemInterval = 40;
    var chartPositionX = "0";
    var chartPositionY = "0";
    var chartWidth = chartPreviewSvgWidth;
    var chartHeight = chartPreviewSvgHeight;
    var moveable = false;
    var myChart = new Chart(chartId, chartDivId, chartSvgId, chartGId, svgCssClass, chartTitle, chartXaxisLine, chartYaxisLine, chartItems, chartItemInterval, chartTitleX, chartTitleY, chartPositionX, chartPositionY, chartWidth, chartHeight, moveable);

    // Add the default Chart Items using parms: itemSeqNbr, itemName, itemNameX, itemNameY, itemNumValue, selectedIconId, selectedIconColorId, iconRowStartX, iconRowStartY
    var itemNameX = "5"; // how many pixels to indent the item names from the left
    var iconRowStartX = myChart.calcIconRowStartX(); // based on the Y axis line of this chart
    var selectedIconId = "";
    var selectedIconColorId = "";
    var itemSeqNbr = "";
    var itemNumValue = "";
    var iconRowStartY = "";
    var itemNameY = "";
    for (var i = 1; i < 4; i++) {
        itemSeqNbr = i.toString();
        var itemName = "Item " + itemSeqNbr;
        iconRowStartY = myChart.calcItemPosY(itemSeqNbr);
        itemNameY = myChart.calcItemPosY(itemSeqNbr);
        myChartItem = new ChartItem(itemSeqNbr, itemName, itemNameX, itemNameY, itemNumValue, selectedIconId, selectedIconColorId, iconRowStartX, iconRowStartY);
        myChart.addChartItem(myChartItem);
    }

    // Set the global current Chart reference to this Chart:
    currentChart = myChart;
    // initialize the chart preview
    init();
    // display the current chart in the preview window
    currentChart.displayChart();
    //displayChart(currentChart.chartDivId, currentChart.chartSvgId, currentChart.svgCssClass, currentChart.chartWidth, currentChart.chartHeight, currentChart.chartPositionX, currentChart.chartPositionY);
    // make the creator panel visible
    openCreatorWindow();
}

function openCreatorWindowUpdate() {
    newChart = false;
    // Make sure we don't use an old current Chart reference. It would be better if we could set currentChart to the chart being updated here.
    //currentChart = null;
    openCreatorWindow();
}

function openCreatorWindow() {
    // change CSS for chart creator from display = "none" to display = "block"
    var chartCreator = document.getElementById("chart-creator");
    chartCreator.style.display = "block"; 

    // old way:
    /* var classString = document.getElementById("chart-creator").className;
    classString = classString.replace(/ off-page/g, "");
    //console.log("Creator window class string: " + classString);
    document.getElementById("chart-creator").className = classString; */

    var mode = "";
    if(newChart) {
        mode = "add";
        console.log("Current Chart ID: " + currentChart.chartId);
    }else{
        mode = "update";
    }
    console.log("Creator window opened in " + mode + " mode.");  
}

// Make Charts Draggable. This solution (selectElement and moveElement functions) was found at: http://www.petercollingridge.co.uk/interactive-svg-components/draggable-svg-element
// *note: Peter specifies this code as ecmascript, not javascript. 
var selectedElement = 0;
var currentX = 0;
var currentY = 0;
var currentMatrix = 0;

function selectElement(evt) {
    // get the target element, and record its X and Y position
    selectedElement = evt.target;
    currentX = evt.clientX;
    currentY = evt.clientY;
    // get the current matrix numbers from the "transform" attribute of the target element (SVG in our case),
    // and store these in an array (of strings):
    currentMatrix = selectedElement.getAttributeNS(null, "transform").slice(7,-1).split(' ');
     
    // change the matrix numbers in our array from string to float:
    for(var i=0; i<currentMatrix.length; i++) {
        currentMatrix[i] = parseFloat(currentMatrix[i]);
    }
    // Add a new event trigger to the element for mouse move events:
    selectedElement.setAttributeNS(null, "onmousemove", "moveElement(evt)");
    selectedElement.setAttributeNS(null, "onmouseup", "deselectElement(evt)");
}

function moveElement(evt){
    // get difference between the new and previous X,Y mouse position:
    dx = evt.clientX - currentX;
    dy = evt.clientY - currentY;
    // update our matrix with the X,Y differences:
    currentMatrix[4] += dx;
    currentMatrix[5] += dy;
    // format the new matrix value of our transform attribute:
    newMatrix = "matrix(" + currentMatrix.join(' ') + ")";
    
    // set the transform attribute of our element (SVG) to the new matrix value, and store the current X,Y mouse position         
    selectedElement.setAttributeNS(null, "transform", newMatrix);
    currentX = evt.clientX;
    currentY = evt.clientY;
}

// Deselect the element on mouse up.
function deselectElement(evt){
    if(selectedElement != 0){
    selectedElement.removeAttributeNS(null, "onmousemove");
    selectedElement.removeAttributeNS(null, "onmouseout");
    selectedElement.removeAttributeNS(null, "onmouseup");
    selectedElement = 0;
    }
}