// Global Variables:
var chartId = 0;
var chartIds = [];
var currentChart;
var chartTitle = "";
var selectedIconId = "";
var selectedIconColorId = "";
var allIconIds = ["icon-1", "icon-2", "icon-3"];
var allIconColorIds = ["icon-color-1", "icon-color-2", "icon-color-3", "icon-color-4"];
var iconSvgWidth = "60";
var iconSvgHeight = "60";
var newChart = true;
var chartEditorDivId = "chart-sandbox";
var chartPreviewDivId = "chart-preview-container";
var chartPreviewSvgId = "chart-preview-svgid";
var chartPreviewGId = "chart-preview-g";
var chartPreviewSvgCssClass = "chart-preview-svg";

var chartPreviewSvgWidth = "400";
var chartPreviewSvgHeight = "401";

var globalChartArray = []; // an array of chart objects that will appear in the editor

var chartDefaultXaxisLine = {
    x1: "91",
    y1: "150",
    x2: "390",
    y2: "150"
};
var chartDefaultYaxisLine = {
    x1: "91",
    y1: "20",
    x2: "91",
    y2: "150"
};

var chartRowInterval = 40;
var chartRowMarkerLng = 10;
var rowLabelIndent = 5;
