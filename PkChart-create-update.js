// Global Variables declared in PkChart-globals.js

function init() {
    // clear form text inputs on page load:
    document.getElementById("item-name").value = "";
    document.getElementById("item-value").value = "";
    document.getElementById("title-textbox").value = "";
    document.getElementById("item-seq-nbr").value = "1";
    document.getElementById("item-value").value = "1";

    // clear any icons that were selected
    for (var i = 0; i < allIconIds.length; i++) {
        document.getElementById(allIconIds[i]).setAttribute("class", "clickable-icon");
    }

    // clear global variables
    chartTitle = "";
    selectedIconId = "";
    selectedIconColorId = "";

    // clear the chart preview
    removeChartPreview();
}
window.onload = init;

// create SVG icons
var circleMarkup = iconCircleSvg(iconSvgWidth, iconSvgHeight, "icon-1", "clickable clickable-icon", "black", "2", "white");
document.getElementById("circle-icon").innerHTML = circleMarkup;
var squareMarkup = iconSquareSvg(iconSvgWidth, iconSvgHeight, "icon-2", "clickable clickable-icon", "black", "2", "white");
document.getElementById("square-icon").innerHTML = squareMarkup;
var triangleMarkup = iconTriangleSvg(iconSvgWidth, iconSvgHeight, "icon-3", "clickable clickable-icon", "black", "2", "white");
document.getElementById("triangle-icon").innerHTML = triangleMarkup;

// Handle User Close Window
document.getElementById("close-creator-button").onclick = closeCreatorWindow;

// Handle Clear Changes
document.getElementById("creator-clear-button").onclick = init;

// Handle User Icon Selection:
document.getElementById("icon-1").onclick = saveIcon1;
document.getElementById("icon-2").onclick = saveIcon2;
document.getElementById("icon-3").onclick = saveIcon3;

// Save the selected icon:
function saveIcon1() { saveIcon("icon-1"); }
function saveIcon2() { saveIcon("icon-2"); }
function saveIcon3() { saveIcon("icon-3"); }

function saveIcon(id) {
    selectedIconId = id;
    console.log("Selected Icon ID: " + selectedIconId);

    // only one icon may be selected, clear any that were previously selected:
    for (var i = 0; i < allIconIds.length; i++) {
        document.getElementById(allIconIds[i]).setAttribute("class", "clickable-icon");
    }

    // If a color has been selected, then assign that color to the icon
    if (selectedIconColorId) {
        document.getElementById(id).setAttribute("class", selectedIconColorId); // color class name is same as id name
    } else {
        // mark the currently selected icon as selected:
        document.getElementById(id).setAttribute("class", "selected-icon");
    }
}

// Handle user icon color selection
function saveIconColor(id) {
    selectedIconColorId = id;
    console.log("Selected Icon Color: " + selectedIconColorId);

    // If an icon has been selected, then assign the selected color to that icon
    if (selectedIconId) {
        document.getElementById(selectedIconId).setAttribute("class", selectedIconColorId); // color class name is same as id name
    }
}

// Chart item submission
document.getElementById("item-update-button").onclick = addChartItem;

function addChartItem() {
    // Get the chart creator/updater form data and populate a new ChartItem object with these. Then add the new ChartItem
    // to the item array in the current Chart object.
    // note: icon and icon-color selections are in global variables: selectedIconId, selectedIconColorId
    var itemSeqNbr = document.getElementById("item-seq-nbr").value;
    console.log("Chart Item Sequence Number: " + itemSeqNbr);

    var itemName = document.getElementById("item-name").value;
    console.log("Chart Item Name: " + itemName);

    var itemNumValue = document.getElementById("item-value").value;
    console.log("Chart Item Numeric Value: " + itemNumValue);

    // update chart title in the current Chart object:
    chartTitle = document.getElementById("title-textbox").value;
    if (chartTitle) {
        currentChart.chartTitle = chartTitle;
    }

    var itemNameX = "5"; // how many pixels to indent the item names from the left
    var iconRowStartX = currentChart.calcIconRowStartX(); // based on the Y axis line of this chart

    var iconRowStartY = currentChart.calcItemPosY(itemSeqNbr);
    var itemNameY = currentChart.calcItemPosY(itemSeqNbr);
    var myChartItem = new ChartItem(itemSeqNbr, itemName, itemNameX, itemNameY, itemNumValue, selectedIconId, selectedIconColorId, iconRowStartX, iconRowStartY);
    currentChart.addChartItem(myChartItem);

    // Use the new ChartItem object to update the chart preview
    updateChartPreview(myChartItem);

}

function updateChartPreview(myChartItem) {
    // Remove the existing chart from the preview window.
    removeChartPreview();

    // Display the current chart
    currentChart.displayChart();
}

function removeChartPreview() {
    // Remove the existing chart from the preview window.
    var myNode = document.getElementById(chartPreviewDivId);
        while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }   
}

function addCircleIconRow(svgParentId, chartItemObj) {
    var numVal = Number(chartItemObj.itemNumValue);
    var cx = chartItemObj.iconRowStartX;
    cx = addToString(cx, 12); // to nudge first circle to the right of the Y axis line
    var cy = chartItemObj.iconRowStartY;
    cy = subtractFromString(cy, 4); // to line up with text better
    var r = 13;
    var stroke = "black";
    var strokeWidth = "2";
    var fill = "white";
    var cssClass = chartItemObj.itemIconColorId;
    for (var i = 0; i < numVal; i++) {
        addSvgCircleNode(svgParentId, cssClass, cx, cy, r, stroke, strokeWidth, fill);
        cx = addToString(cx, 30);
    }
}

function addSquareIconRow(svgParentId, chartItemObj) {
    var numVal = Number(chartItemObj.itemNumValue);
    var x = chartItemObj.iconRowStartX;
    var y = chartItemObj.iconRowStartY;
    y = subtractFromString(y, 16); // to line up with text better
    var width = "24";
    var height = "24";
    var stroke = "black";
    var strokeWidth = "2";
    var fill = "white";
    var cssClass = chartItemObj.itemIconColorId;
    for (var i = 0; i < numVal; i++) {
        addSvgRectNode(svgParentId, cssClass, x, y, width, height, stroke, strokeWidth, fill);
        x = addToString(x, 30);
    }    
}

function addTriangleIconRow(svgParentId, chartItemObj) {
    var numVal = Number(chartItemObj.itemNumValue);
    var x = chartItemObj.iconRowStartX;
    var y = chartItemObj.iconRowStartY;
    y = subtractFromString(y, 18);
    //y = subtractFromString(y, 16); // to line up with text better
    var triPoints = {  // some default values for reference
        x1: "13",
        y1: "2",
        x2: "24",
        y2: "24",
        x3: "2",
        y3: "24"
    }
    var points = triPoints.x1 + ',' + triPoints.y1 + ' ' + triPoints.x2 + ',' + triPoints.y2 + ' ' + triPoints.x3 + ',' + triPoints.y3; 
    var stroke = "black";
    var strokeWidth = "2";
    var fill = "white";
    var cssClass = chartItemObj.itemIconColorId;
    var childSvgId = "";
    var childSvgWidth = "26";
    var childSvgHeight = "26";
    var mySvgParentNode = document.getElementById(svgParentId);
    for (var i = 0; i < numVal; i++) {
        // create a new svg node
        var mySvgNode = newSvgNode("", "", childSvgWidth, childSvgHeight, x, y);
        // create the triangle as a new polygon node
        var myPolyNode = newSvgPolyNode(cssClass, points, stroke, strokeWidth, fill);
        // Add the triangle as child of the new "child" svg, then add new svg as child to the parent (preview) svg
        mySvgNode.appendChild(myPolyNode);
        mySvgParentNode.appendChild(mySvgNode);
        // Increment x to make room for the next icon
        x = addToString(x, 29.8);
    }    
}

function newSvgPolyNode(cssClass, points, stroke, strokeWidth, fill) {
// <polygon class="clickable clickable-icon" points="30,10 50,50 10,50" stroke="black" stroke-width="2" fill="white" />
    var myNode = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
    myNode.setAttribute("class", cssClass);
    myNode.setAttribute("points", points);
    myNode.setAttribute("stroke", stroke);
    myNode.setAttribute("stroke-width", strokeWidth);
    myNode.setAttribute("fill", fill);
    return myNode;
}


function addSvgPolyNode(svgParentId, cssClass, points, stroke, strokeWidth, fill) {
// <polygon class="clickable clickable-icon" points="30,10 50,50 10,50" stroke="black" stroke-width="2" fill="white" />
    var mySvg = document.getElementById(svgParentId);
    var myNode = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
    myNode.setAttribute("class", cssClass);
    myNode.setAttribute("points", points);
    myNode.setAttribute("stroke", stroke);
    myNode.setAttribute("stroke-width", strokeWidth);
    myNode.setAttribute("fill", fill);
    mySvg.appendChild(myNode);
}

function addSvgRectNode(svgParentId, cssClass, x, y, width, height, stroke, strokeWidth, fill) {
    var mySvg = document.getElementById(svgParentId);
    var myNode = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    myNode.setAttribute("class", cssClass);
    myNode.setAttribute("x", x);
    myNode.setAttribute("y", y);
    myNode.setAttribute("width", width);
    myNode.setAttribute("height", height);
    myNode.setAttribute("stroke", stroke);
    myNode.setAttribute("stroke-width", strokeWidth);
    myNode.setAttribute("fill", fill);
    mySvg.appendChild(myNode);
}

function addSvgCircleNode(svgParentId, cssClass, cx, cy, r, stroke, strokeWidth, fill) {
    var mySvg = document.getElementById(svgParentId);
    var myNode = document.createElementNS("http://www.w3.org/2000/svg", "circle");
    myNode.setAttribute("class", cssClass);
    myNode.setAttribute("cx", cx);
    myNode.setAttribute("cy", cy);
    myNode.setAttribute("r", r);
    myNode.setAttribute("stroke", stroke);
    myNode.setAttribute("stroke-width", strokeWidth);
    myNode.setAttribute("fill", fill);
    mySvg.appendChild(myNode);
}

function newSvgNode(svgNodeId, svgNodeClass, svgWidth, svgHeight, x, y) {
    // Create a new SVG node and return it 
    var mySvgNode = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    mySvgNode.setAttribute("xmlns", "http://www.w3.org/2000/svg");
    mySvgNode.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
    mySvgNode.setAttribute("id", svgNodeId);
    mySvgNode.setAttribute("class", svgNodeClass);
    mySvgNode.setAttribute("width", svgWidth);
    mySvgNode.setAttribute("height", svgHeight);
    mySvgNode.setAttribute("x", x);
    mySvgNode.setAttribute("y", y);
    return mySvgNode;
/* Creates a node like: <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
    id="chart-preview-svg" width="400" height="301"></svg> */
}

function replaceSvgChild(parentNodeId, svgNodeId, svgNodeClass, svgWidth, svgHeight) {
    // Create the SVG node under a known parent node:
    var myParentNode = document.getElementById(parentNodeId);
    var myOldSvgNode = document.getElementById(svgNodeId);
    var mySvgNode = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    mySvgNode.setAttribute("xmlns", "http://www.w3.org/2000/svg");
    mySvgNode.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
    mySvgNode.setAttribute("id", svgNodeId);
    mySvgNode.setAttribute("class", svgNodeClass);
    mySvgNode.setAttribute("width", svgWidth);
    mySvgNode.setAttribute("height", svgHeight);
    myParentNode.replaceChild(mySvgNode, myOldSvgNode);
/* Replaces a node like: <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
    id="chart-preview-svg" width="400" height="301"></svg> */
}


function addSvgLineNode(svgParentId, childClass, childAttributesObj) {
    // create an SVG child line node like: <line id="chart-Y-axis-line" class="chart-lines" x1="91" y1="0" x2="91" y2="300" />
    // note: innerHTML doesn't work with SVG elements
    var mySvg = document.getElementById(svgParentId);
    var myNode = document.createElementNS("http://www.w3.org/2000/svg", "line");
    myNode.setAttribute("class", childClass);
    myNode.setAttribute("x1", childAttributesObj.x1);
    myNode.setAttribute("y1", childAttributesObj.y1);
    myNode.setAttribute("x2", childAttributesObj.x2);
    myNode.setAttribute("y2", childAttributesObj.y2);
    mySvg.appendChild(myNode);    
}

function addSvgTextNode(svgParentId, childClass, x, y, textString) {
    //create an SVG child text node like: <text id="item-name-1" class="chart-text" x="5" y="50" >item 1</text>
    // note: innerHTML doesn't work with SVG elements
    var mySvg = document.getElementById(svgParentId);
    var myNode = document.createElementNS("http://www.w3.org/2000/svg", "text");
    myNode.setAttribute("class", childClass);
    myNode.setAttribute("x", x);
    myNode.setAttribute("y", y);
    var textContent = document.createTextNode(textString);
    myNode.appendChild(textContent);
    mySvg.appendChild(myNode);   
}

// Create SVG Markup without child nodes
function basicSvgMarkup(svgId, className, svgWidth, svgHeight) {
    var svgMarkup = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="' + svgId + '" class="' + className + '" width="' + svgWidth + '" height="' + svgHeight + '" ></svg>';
    return svgMarkup;
}

// Create SVG Markup for icons
function iconCircleSvg(svgWidth, svgHeight, id, className, stroke, strokeWidth, fill) {
    var svgMarkup = '<svg width="' + svgWidth + '" height="' + svgHeight + '" ><circle id="' + id + '" class="' + className + '" cx="25" cy="30" r="21" stroke="' + stroke + '" stroke-width="' + strokeWidth + '" fill="' + fill + '" /></svg>';
    return svgMarkup;
}

function iconSquareSvg(svgWidth, svgHeight, id, className, stroke, strokeWidth, fill) {
    //Like: <svg height="60" width="60"><rect id="icon-2" class="clickable clickable-icon" x="10" y="10" width="40" height="40" stroke="black" stroke-width="2" fill="white" /></svg>
    var svgMarkup = '<svg width="' + svgWidth + '" height="' + svgHeight + '" ><rect id="' + id + '" class="' + className + '" x="10" y="10" width="40" height="40" stroke="' + stroke + '" stroke-width="' + strokeWidth + '" fill="' + fill + '" /></svg>';
    return svgMarkup;
}

function iconTriangleSvg(svgWidth, svgHeight, id, className, stroke, strokeWidth, fill) {
    //Like: <svg height="60" width="60"><polygon id="icon-3" class="clickable clickable-icon" points="30,10 50,50 10,50" stroke="black" stroke-width="2" fill="white" /></svg>
    var svgMarkup = '<svg width="' + svgWidth + '" height="' + svgHeight + '" ><polygon id="' + id + '" class="' + className + '" points="30,10 50,50 10,50" stroke="' + stroke + '" stroke-width="' + strokeWidth + '" fill="' + fill + '" /></svg>';
    return svgMarkup;
}


function closeCreatorWindow() {
    // change chart creator CSS from display = "block" to display = "none"
    var chartCreator = document.getElementById("chart-creator");
    chartCreator.style.display = "none";
    //document.getElementById("chart-creator").className += " off-page";
}


function subtractFromString(str, numToSubtract) {
    var x = Number(str);
    x = (x - numToSubtract);
    str = x.toString();
    return str;
}

function addToString(str, numToAdd) {
    var x = Number(str);
    x = (x + numToAdd);
    str = x.toString();
    return str;
}

function multiplyString(str, multiplier) {
    var x = Number(str);
    x = (x * multiplier);
    str = x.toString();
    return str;        
}
