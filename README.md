chart-maker
===========

a pictogram chart creating/editing tool that uses svg images

This project uses one web page, index.html, which has markup for the editor canvas and the creator/updater panel.
The intention is that when you click the "Add" or "Update" button in the editor, the creator/updater panel appears.
There you create or update a pictogram chart using the form controls on the right. The controls apply to one chart item
which is one horizontal row on the chart consisting of a name/label, and a number of icons progressing from the Y axis
to the right depending on the numeric value supplied in the form. The type of icon and icon color for that row is also 
selected in the form. 
